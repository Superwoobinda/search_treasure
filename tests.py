import pytest

from marshmallow import ValidationError

from config import *
from treasure_func import (
    validate_matrix_data,
    create_matrix,
    steps_to_treasure,
    search_treasure
)
from treasure_oop import (
    TreasureMap,
    MatrixValidateSchema,
    TreasureSearcher
)


# Tests of functional implementation

def test_validate_matrix_data():
    data = [55, 14, 25, 52, 21, 44, 31, 11, 53, 43, 24, 13, 45, 12, 34, 42, 22, 43, 32, 41, 51, 23, 33, 54, 15]
    result = validate_matrix_data(data)
    assert result == data

    with pytest.raises(Exception) as e:
        validate_matrix_data([])
    assert "Length of matrix must be equal to 25" in str(e.value)

    with pytest.raises(Exception) as e:
        validate_matrix_data(list(range(1, 100)))
    assert f"Length of matrix must be equal to {MATRIX_LENGTH}" in str(e.value)

    with pytest.raises(Exception) as e:
        validate_matrix_data(list(range(1, 25 + 1)))
    assert f"Values in given data must be in range {MATRIX_MIN_VALUE} - {MATRIX_MAX_VALUE}" in str(e.value)

    with pytest.raises(Exception) as e:
        validate_matrix_data([float(value) for value in data])  # noqa
    assert f"Values in given data must be of type Integer, not {float.__name__}" in str(e.value)


def test_create_matrix():
    data = [55, 14, 25, 52, 21, 44, 31, 11, 53, 43, 24, 13, 45, 12, 34, 42, 22, 43, 32, 41, 51, 23, 33, 54, 15]
    mock_result = [
        [55, 14, 25, 52, 21],
        [44, 31, 11, 53, 43],
        [24, 13, 45, 12, 34],
        [42, 22, 43, 32, 41],
        [51, 23, 33, 54, 15]
    ]
    result = create_matrix(data)
    assert result == mock_result


def test_do_next_step():
    matrix = [
        [55, 14, 25, 52, 21],
        [44, 31, 11, 53, 43],
        [24, 13, 45, 12, 34],
        [42, 22, 43, 32, 41],
        [51, 23, 33, 54, 15]
    ]
    mock_result = [55, 15, 21, 44, 32, 13, 25, 43]
    result = steps_to_treasure(matrix, MATRIX_START_CELL[0], MATRIX_START_CELL[1])
    assert result == mock_result


def test_search_treasure():
    data = [55, 14, 25, 52, 21, 44, 31, 11, 53, 43, 24, 13, 45, 12, 34, 42, 22, 43, 32, 41, 51, 23, 33, 54, 15]
    mock_result = [55, 15, 21, 44, 32, 13, 25, 43]
    result = search_treasure(data)
    assert result == mock_result


# Tests of OOP implementation

class TestTreasureSearcher:
    data = [
        55, 14, 25, 52, 21,
        44, 31, 11, 53, 43,
        24, 13, 45, 12, 34,
        42, 22, 43, 32, 41,
        51, 23, 33, 54, 15
    ]
    matrix_validator = MatrixValidateSchema
    treasure_map = TreasureMap(
        MATRIX_ROW_LENGTH=MATRIX_ROW_LENGTH, MATRIX_COL_LENGTH=MATRIX_COL_LENGTH,
        MATRIX_MIN_VALUE=MATRIX_MIN_VALUE, MATRIX_MAX_VALUE=MATRIX_MAX_VALUE,
        MATRIX_LENGTH=MATRIX_LENGTH,
        ValidateSchema=matrix_validator,
        data=data
    )
    treasure_searcher = TreasureSearcher(treasure_map, MATRIX_START_CELL)
    treasure_seek_result = [55, 15, 21, 44, 32, 13, 25, 43]
    mock_matrix = [
        [55, 14, 25, 52, 21],
        [44, 31, 11, 53, 43],
        [24, 13, 45, 12, 34],
        [42, 22, 43, 32, 41],
        [51, 23, 33, 54, 15]
    ]
    get_matrix_number_mock_result = 55
    second_step_mock_cell = (5, 5)
    last_step_mock_cell = (4, 3)

    def test_treasure_map_create_matrix(self):
        assert self.treasure_map._matrix == self.mock_matrix

    def test_treasure_map__str__(self):
        mock__str__result = "\n".join(str(row) for row in self.mock_matrix)
        assert self.treasure_map.__str__() == mock__str__result

    def test_treasure_map_get_matrix_number(self):
        assert self.treasure_map.get_matrix_number(row_index=1, col_index=1) == self.get_matrix_number_mock_result

    def test_matrix_validate_schema(self):
        validated_data = self.matrix_validator().load({"data": self.data})
        assert validated_data["data"] == self.data

        with pytest.raises(ValidationError) as e:
            self.matrix_validator().load({"data": list(range(1,20))})
        assert "Length of matrix must be equal to 25" in str(e.value)

        with pytest.raises(ValidationError) as e:
            self.matrix_validator().load({"data": list(range(1, 25 + 1))})
        assert f"Values in given data must be in range {MATRIX_MIN_VALUE} - {MATRIX_MAX_VALUE}" in str(e.value)

    def test_treasure_searcher_do_next_step(self):
        self.treasure_searcher.do_next_step()
        assert (self.treasure_searcher.row_index, self.treasure_searcher.col_index) == self.second_step_mock_cell
        assert self.treasure_searcher.search_is_completed == False

    def test_treasure_searcher_seek_treasure(self):
        assert self.treasure_searcher.seek_treasure() == self.treasure_seek_result
        assert (self.treasure_searcher.row_index, self.treasure_searcher.col_index) == self.last_step_mock_cell
        assert self.treasure_searcher.search_is_completed == True
