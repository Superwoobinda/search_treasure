from typing import List, Union

from config import *

####################### Functional implementation ##################

LIST_OR_NONE = Union[List, None]

def validate_matrix_data(data: List[int]) -> List[int]:
    """Validate given data before create matrix."""
    if len(data) != MATRIX_LENGTH:
        raise Exception(f"Length of matrix must be equal to {MATRIX_LENGTH}")

    for value in data:
        if not isinstance(value, int):
            raise Exception(
                f"Values in given data must be of type Integer, not {type(value).__name__}")

        if value not in range(MATRIX_MIN_VALUE, MATRIX_MAX_VALUE + 1):
            raise Exception(
                f"Values in given data must be in range {MATRIX_MIN_VALUE} - {MATRIX_MAX_VALUE}")

    return data


def create_matrix(validated_data: List[int]) -> List[List[int]]:
    """Create matrix from validated data."""
    matrix = []
    while validated_data:
        matrix.append(validated_data[:MATRIX_ROW_LENGTH])
        validated_data = validated_data[MATRIX_ROW_LENGTH:]

    return matrix


def steps_to_treasure(matrix: List[List[int]],
                      row_index: int,
                      col_index: int,
                      steps_taken: LIST_OR_NONE=None
                      ) -> List[int]:
    """
    Take the next step to find the treasure.
    If the treasure was found - returns an array of steps taken.
    """
    found_value = matrix[row_index - 1][col_index - 1]

    if not steps_taken:
        steps_taken = []
    else:
        if found_value == steps_taken[-1]:
            return steps_taken

    steps_taken.append(found_value)
    next_row_index, next_col_index = int(str(found_value)[0]), int(str(found_value)[1])

    return steps_to_treasure(matrix, next_row_index, next_col_index, steps_taken)


def search_treasure(data: List[int]) -> List[int]:
    """Preparing matrix and processing steps for find a treasure."""
    validated_data = validate_matrix_data(data)
    matrix = create_matrix(validated_data)
    result = steps_to_treasure(matrix, MATRIX_START_CELL[0], MATRIX_START_CELL[1])

    return result


if __name__ == '__main__':
    data = [55, 14, 25, 52, 21,
            44, 31, 11, 53, 43,
            24, 13, 45, 12, 34,
            42, 22, 43, 32, 41,
            51, 23, 33, 54, 15]

    result = search_treasure(data)
    print("In functional: %s" % result)
