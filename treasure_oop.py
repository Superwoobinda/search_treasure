from typing import Tuple, List
from marshmallow import Schema, fields, validates, ValidationError

from config import *

#################### OOP Implementation ##################

class TreasureMap:
    def __init__(self,
                 MATRIX_ROW_LENGTH: int, MATRIX_COL_LENGTH: int,
                 MATRIX_MIN_VALUE: int, MATRIX_MAX_VALUE: int,
                 MATRIX_LENGTH: int,
                 ValidateSchema: Schema,
                 data: List[int]
                 ) -> None:

        self.MATRIX_ROW_LENGTH = MATRIX_ROW_LENGTH
        self.MATRIX_COL_LENGTH = MATRIX_COL_LENGTH
        self.MATRIX_MIN_VALUE = MATRIX_MIN_VALUE
        self.MATRIX_MAX_VALUE = MATRIX_MAX_VALUE

        self.set_validator(ValidateSchema)
        self.create_matrix(data)

    def set_validator(self, ValidateSchema: Schema) -> None:
        if not issubclass(ValidateSchema, Schema):
            raise ValidationError(
                "ValidateSchema must be type of marshmallow.Schema")

        self._validator = ValidateSchema

    def create_matrix(self, data: List[int]) -> None:
        validated_data = self._validator().load({"data": data})  # noqa
        validated_data = validated_data["data"]
        matrix = []

        while validated_data:
            matrix.append(validated_data[:self.MATRIX_ROW_LENGTH])
            validated_data = validated_data[self.MATRIX_ROW_LENGTH:]

        self._matrix = matrix

    def get_matrix_number(self, row_index: int, col_index: int) -> int:
        return self._matrix[row_index - 1][col_index - 1]

    def __str__(self):
        return "%s" % "\n".join(str(row) for row in self._matrix)


class MatrixValidateSchema(Schema):
    data = fields.List(fields.Integer(), required=True)

    @validates("data")
    def validate_data(self, data: List[int]) -> List[int]:
        if len(data) != MATRIX_LENGTH:
            raise ValidationError(f"Length of matrix must be equal to {MATRIX_LENGTH}")

        for value in data:
            if value not in range(MATRIX_MIN_VALUE, MATRIX_MAX_VALUE + 1):
                raise ValidationError(
                    f"Values in given data must be in range {MATRIX_MIN_VALUE} - {MATRIX_MAX_VALUE}")

        return data


class TreasureSearcher:
    def __init__(self,
                 matrix: TreasureMap,
                 MATRIX_START_CELL: Tuple[int, int]
                 ) -> None:

        self.matrix = matrix
        self.row_index = MATRIX_START_CELL[0]
        self.col_index = MATRIX_START_CELL[1]
        self.steps_taken = []
        self.search_is_completed = False

    def seek_treasure(self) -> List[int]:
        while not self.search_is_completed:
            self.do_next_step()

        return self.steps_taken

    def do_next_step(self) -> None:
        next_matrix_number = self.matrix.get_matrix_number(self.row_index, self.col_index)

        if self.steps_taken and next_matrix_number == self.steps_taken[-1]:
            self.search_is_completed = True
        else:
            self.steps_taken.append(next_matrix_number)
            self.row_index = int(str(next_matrix_number)[0])
            self.col_index = int(str(next_matrix_number)[1])


if __name__ == '__main__':
    data = [55, 14, 25, 52, 21,
            44, 31, 11, 53, 43,
            24, 13, 45, 12, 34,
            42, 22, 43, 32, 41,
            51, 23, 33, 54, 15]

    matrix = TreasureMap(
        MATRIX_ROW_LENGTH=MATRIX_ROW_LENGTH, MATRIX_COL_LENGTH=MATRIX_COL_LENGTH,
        MATRIX_MIN_VALUE=MATRIX_MIN_VALUE, MATRIX_MAX_VALUE=MATRIX_MAX_VALUE,
        MATRIX_LENGTH=MATRIX_LENGTH,
        ValidateSchema=MatrixValidateSchema,
        data=data
    )

    treasure_searcher = TreasureSearcher(matrix, MATRIX_START_CELL)
    result = treasure_searcher.seek_treasure()
    print("In OOP: %s" % result)
